# Telegraf MySQL Session Module

Telegraf MySQL Session Module is a robust session management middleware for Telegraf.js bots that seamlessly integrates with MySQL databases. This comprehensive README provides detailed instructions on installation, usage, API documentation, error handling, and licensing information.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [API](#api)
  - [Constructor](#constructor)
  - [Methods](#methods)
  - [Middleware](#middleware)
- [Error Handling](#error-handling)
- [License](#license)

## Installation

To install the Telegraf MySQL Session Module, use npm:

```bash
npm install telegraf-mysql2-session
```

## Usage

Integrate the Telegraf MySQL Session Module into your Telegraf bot with the following steps:

1. **Require the module and initialize MySQLSession**:

```javascript
const { Telegraf } = require('telegraf');
const MySQLSession = require('telegraf-mysql2-session');

const session = new MySQLSession({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'sessions',
});
```

2. **Add the middleware to your Telegraf bot**:

```javascript
const bot = new Telegraf('shhh... secret');
bot.use(session.middleware());
```

3. **Access and modify session data using `ctx.session`**:

```javascript
bot.on('text', (ctx) => {
  // Set session value
  ctx.session.counter = (ctx.session.counter || 0) + 1;
  // Get session value
  ctx.reply(`Counter: ${ctx.session.counter}`);
});

bot.launch();
```

For detailed API documentation and advanced usage, refer to the [API](#api) section below.

## API

### Constructor

Create a new instance of MySQLSession with the following options:

```javascript
new MySQLSession(options)
```

- `options`: An object containing MySQL connection options and session configuration.

#### Options

- `host`: MySQL server host (default: 'localhost').
- `user`: MySQL user (default: 'root').
- `password`: MySQL password (default: null).
- `database`: MySQL database name (default: 'telegraf_sessions').
- `connectionLimit`: Maximum number of connections in the pool (default: 10).
- `cleanupInterval`: Interval (in milliseconds) for cleaning up expired sessions (default: 3600000).
- `sessionsTableName`: Name of the table to store sessions (default: 'bejiihiu_sessions').

### Methods

#### `saveSession(sessionId, sessionData, expirationTime)`

Save session data to the database.

- `sessionId`: Unique identifier for the session.
- `sessionData`: Data to be stored in the session.
- `expirationTime`: Expiration time for the session data in seconds (default: 3600).

#### `getSession(sessionId)`

Retrieve session data from the database.

- `sessionId`: Unique identifier for the session.

### Middleware

Generate Telegraf middleware function for session management.

```javascript
session.middleware()
```

## Error Handling

Errors during session creation, cleanup, or database queries are logged to the console. Customize error handling logic as needed.

## License

This project is licensed under the MIT License. See the [LICENSE](https://mit-license.org/) file for details.